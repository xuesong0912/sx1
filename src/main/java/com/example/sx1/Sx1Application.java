package com.example.sx1;


import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import static com.alibaba.nacos.api.config.ConfigType.YAML;

@SpringBootApplication
@NacosPropertySource(dataId = "configTest",type = YAML,autoRefreshed = true)
public class Sx1Application {

    public static void main(String[] args) {
        SpringApplication.run(Sx1Application.class, args);
    }

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
