package com.example.sx1.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/test")
@RestController
public class testController {

    @NacosValue(value = "${test.config}",autoRefreshed = true)
    private String testConfig;

    @GetMapping("/test")
    public String test(){
        return "sx1";
    }

    @GetMapping("/sql")
    public String testSql(){
        return testConfig;
    }
}
